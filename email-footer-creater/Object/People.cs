﻿using System;
using System.Collections.Generic;

namespace email_footer_creater.Object
{
    public class People
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public Dictionary<string, string> Attributes = new Dictionary<string, string>();
    }
}