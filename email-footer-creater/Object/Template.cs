﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Object
{
    public class Template
    {
        public List<People> People = new List<People>();
        public string DataSetPath {get; set;}
        public string Path { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public Dictionary<string,string> Tag { get; set; }

        public override string ToString()
        {
           return Name;
        }

    }


}
