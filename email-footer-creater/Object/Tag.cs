﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Object
{
    public class Tag
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
