﻿namespace email_footer_creater
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.btnGenerator = new System.Windows.Forms.Button();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.downloadsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.templateEmailFooterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.Label();
            this.lblGen = new System.Windows.Forms.Label();
            this.lblTemplate = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(6, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(222, 31);
            this.label8.TabIndex = 24;
            this.label8.Text = "Footer Generator";
            // 
            // btnGenerator
            // 
            this.btnGenerator.Location = new System.Drawing.Point(4, 202);
            this.btnGenerator.Name = "btnGenerator";
            this.btnGenerator.Size = new System.Drawing.Size(269, 37);
            this.btnGenerator.TabIndex = 32;
            this.btnGenerator.Text = "Generate Footer";
            this.btnGenerator.UseVisualStyleBackColor = true;
            this.btnGenerator.Click += new System.EventHandler(this.btnGenerator_Click);
            // 
            // btnTemplate
            // 
            this.btnTemplate.Location = new System.Drawing.Point(7, 77);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(269, 34);
            this.btnTemplate.TabIndex = 35;
            this.btnTemplate.Text = "Load new template";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(282, 24);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // downloadsToolStripMenuItem
            // 
            this.downloadsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.templateCSVToolStripMenuItem,
            this.templateEmailFooterToolStripMenuItem});
            this.downloadsToolStripMenuItem.Name = "downloadsToolStripMenuItem";
            this.downloadsToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.downloadsToolStripMenuItem.Text = "Downloads";
            // 
            // templateCSVToolStripMenuItem
            // 
            this.templateCSVToolStripMenuItem.Name = "templateCSVToolStripMenuItem";
            this.templateCSVToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.templateCSVToolStripMenuItem.Text = "Template Data Set";
            this.templateCSVToolStripMenuItem.Click += new System.EventHandler(this.templateCSVToolStripMenuItem_Click);
            // 
            // templateEmailFooterToolStripMenuItem
            // 
            this.templateEmailFooterToolStripMenuItem.Name = "templateEmailFooterToolStripMenuItem";
            this.templateEmailFooterToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.templateEmailFooterToolStripMenuItem.Text = "Template Email Footer";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(4, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(269, 34);
            this.button1.TabIndex = 37;
            this.button1.Text = "Load Data Set";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(4, 124);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(86, 13);
            this.lblData.TabIndex = 38;
            this.lblData.Text = "Dataset: Loaded";
            // 
            // lblGen
            // 
            this.lblGen.AutoSize = true;
            this.lblGen.Location = new System.Drawing.Point(4, 186);
            this.lblGen.Name = "lblGen";
            this.lblGen.Size = new System.Drawing.Size(79, 13);
            this.lblGen.TabIndex = 39;
            this.lblGen.Text = "Generate: Start";
            // 
            // lblTemplate
            // 
            this.lblTemplate.AutoSize = true;
            this.lblTemplate.Location = new System.Drawing.Point(7, 61);
            this.lblTemplate.Name = "lblTemplate";
            this.lblTemplate.Size = new System.Drawing.Size(93, 13);
            this.lblTemplate.TabIndex = 40;
            this.lblTemplate.Text = "Template: Loaded";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.lblTemplate);
            this.Controls.Add(this.lblGen);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnTemplate);
            this.Controls.Add(this.btnGenerator);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Footer Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnGenerator;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblGen;
        private System.Windows.Forms.ToolStripMenuItem downloadsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem templateCSVToolStripMenuItem;
        private System.Windows.Forms.Label lblTemplate;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem templateEmailFooterToolStripMenuItem;
    }
}

