﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater
{
    public class Consts
    {
        #region DataSource

        //Datasource folder & Naming Convention
        public const string DataSource = @"email-footer-creator";

        //Naming Convention
        public const string DataSourceTemplate = "{0}_{1}.{2}";
        #endregion

        #region Backups

        /// <summary>
        /// Backups can be setup in this application and are commonly placed in the backup directory with the following naming convention.
        /// </summary>

        //directory
        public const string BackupDirectory = @"backup";

        //Naming Conventions
        public static string BackupFileNameTemplate = "{0}\\{1}_{2}{3}.{4}";
        public const string BackupFileNamePrefix = "backup_";
        #endregion


        #region Logger
        public static string LoggerDirectory = @"logger";
        public static string LoggerFilename = $"log.txt";
        #endregion

        #region Settings
        public static string Settings = @"settings";
        #endregion

        #region SQLite

        //Database Name
        public const string SQLiteFile = "projects.sqlite";

        //Insert Statment
        public const string SQLiteInsert = "INSERT INTO {0} ({1}) VALUES ({2})";

        //Delete By Id Statment
        public const string SQLiteDelete = "DELETE FROM {0} WHERE Id = {1}";

        //Select Statment
        public const string SQLiteSelect = "SELECT * FROM {0}";

        //Create Table Statment
        public const string SQLiteCreate = "CREATE TABLE IF NOT EXISTS {0} ({1})";

        //check if table exists
        public const string SQLiteCheckTableExists = "PRAGMA table_info({0})";
        #endregion

        #region Emails

        //HTML Templates
        public static string Hyperlink = "<a style=\"{0}\" href=\"{1}\">{2}</a>";

        #endregion

        #region Import Data Screen -  Data Includes
        public static bool ShowFileInfo = true;
        public static bool ShowDataInfo = true;
        public static bool ShowDataInfo_TotalDuplicateObjectsInImport = true;
        public static bool ShowDataInfo_TotalUniqueObjectsInImport = true;
        public static bool ShowDataInfo_TotalObjectsBeforeImport = true;
        public static bool ShowDataInfo_TotalObjectsAfterImport = true;

        public static string Template = "templates";
        public static string Output = "footer";
        public static string DataSet = "data";
        #endregion

    }
}
