﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Enums
{
    public enum FileSuffixEnum
    {
        [Description("Daily")]
        Daily,
        [Description("Changes")]
        Changes,
    }
}
