﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Enums
{
    public enum DataSourceTypeEnum
    {
        [Description("json")]
        JSON = 1,
        [Description("xml")]
        XML = 2,
        [Description("csv")]
        CSV = 3,
        [Description("sqlite")]
        SQLITE = 4,
    }

    public enum DataSourceExtensionTypeEnum
    {
        [Description(".json")]
        JSON = 1,
        [Description(".xml")]
        XML = 2,
        [Description(".csv")]
        CSV = 3,
        [Description(".sqlite")]
        SQLITE = 4,
    }
}
