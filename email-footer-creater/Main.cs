﻿using email_footer_creater.Enums;
using email_footer_creater.Helpers;
using email_footer_creater.Object;
using email_footer_creater.Services;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace email_footer_creater
{
    public partial class Main : Form
    {
        public readonly ContextService _dataService;
        public readonly ProcessService _processService;
        public string TemplatePath;
        public string DataPath;
        public List<string> DataTemplateList = new List<string>();

        public Main()
        {
            InitializeComponent();
            _dataService = new ContextService();
            _processService = new ProcessService();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblData.Text = "Data - Not Loaded!";
            lblTemplate.Text = "Template - Not Loaded!";
            lblGen.Text = "Generation - Not Ready!";

            CheckDirExist(Consts.Template);
            CheckDirExist(Consts.DataSet);
            CheckDirExist(Consts.Output);

        }

        private void CheckDirExist(string dir)
        {
            if (!Directory.Exists($@"{dir}"))
            {
                Directory.CreateDirectory(dir);
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnGenerator_Click(object sender, EventArgs e)
        {
            if (DataPath != null && TemplatePath != null)
            {
                //get template
                var selectedTemplate = new Template()
                {
                    Path = TemplatePath,
                    DataSetPath = DataPath
                };

                //load dataset
                selectedTemplate.People = new List<People>();

                selectedTemplate.People = _dataService.Load(typeof(People), Consts.DataSet, DataSourceTypeEnum.CSV);

                //get all template text
                var html = File.ReadAllText(selectedTemplate.Path);

                int count = 0;

                ZipFile zip = new ZipFile();

                foreach (var person in selectedTemplate.People)
                {
                    var currName = "";

                    foreach (var personAttributes in person.Attributes)
                    {
                        if (html.Contains("{{" + personAttributes.Key.ToLower() + "}}"))
                        {
                            //i.GetValue(p.value as Template)
                            html = html.Replace("{{" + personAttributes.Key.ToLower() + "}}", personAttributes.Value.ToString());
                            if(personAttributes.Key.ToLower() == "name")
                            {
                                currName = personAttributes.Value ?? "unknown"+count;
                            }
                        }

                    }
                    count++;

                    zip.AddEntry($@"/"+currName + "_" + DateTime.Now.ToString("dd-mm-yyyy") + ".html", html, Encoding.UTF8);

                }

                zip.Save(Consts.Output + "/footers_" + count.ToString() + DateTime.Now.ToString("dd-mm-yyyy-hh-mm-ss") + ".zip");

                lblGen.Text = "Generation - Complete";
            }
        }

        public void DeleteDirectory(string targetDir)
        {
            File.SetAttributes(targetDir, FileAttributes.Normal);

            string[] files = Directory.GetFiles(targetDir);
            string[] dirs = Directory.GetDirectories(targetDir);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            Directory.Delete(targetDir, false);
        }


        public static void ZipDirInfo(string filePath, string savePath)
        {
            using (var zip = new ZipFile())
            {
                zip.AddDirectory(filePath, $@"/");
                zip.Save(savePath);
            }
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            //get the HTML Template
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                TemplatePath = openFileDialog1.FileName;

                using (StreamReader sr = new StreamReader(openFileDialog1.FileName))
                {
                    DataTemplateList = GetSubStrings(sr.ReadToEnd(), "{{", "}}").ToList();
                }
            }

            var res = MessageBox.Show("Generate Data Template for email?","Data Template",MessageBoxButtons.YesNo);

            if (res == DialogResult.Yes)
            {

                SaveFileDialog save = new SaveFileDialog
                {
                    Filter = "Excel(*.csv) | *.csv",
                    FilterIndex = 2,
                    RestoreDirectory = true
                };
                if (save.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(save.OpenFile()))
                    {
                        if (save.FileName != "")
                        {
                            writer.Write(string.Join(",", DataTemplateList.Distinct()));
                            writer.Flush();
                        }
                        writer.Dispose();
                    }
                }
                else
                {
                    //TODO: extend exception class to create custom eror meszsage message.
                }
            }

            lblTemplate.Text = "Template - Complete";
        }
        private IEnumerable<string> GetSubStrings(string input, string start, string end)
        {
            Regex r = new Regex(Regex.Escape(start) + "(.*?)" + Regex.Escape(end));
            MatchCollection matches = r.Matches(input);
            foreach (Match match in matches)
            {
                yield return match.Groups[1].Value;
            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //data set load
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                Title = "Browse CSV Files",

                CheckFileExists = true,
                CheckPathExists = true,

                DefaultExt = "csv",
                Filter = "csv files (*.csv)|*.csv",
                FilterIndex = 2,
                RestoreDirectory = true,

                ReadOnlyChecked = true,
                ShowReadOnly = true
            };
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DataPath = openFileDialog1.FileName;
            }
            lblData.Text = "Data - Complete";

        }

        private void templateCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string downloadsPath = KnownFolders.GetPath(KnownFolder.Downloads);

            SaveFileDialog save = new SaveFileDialog
            {
                Filter = "Excel(*.csv) | *.csv",
                FilterIndex = 2,
                RestoreDirectory = true
            };
            if (save.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter writer = new StreamWriter(save.OpenFile()))
                {
                    if (save.FileName != "")
                    {
                        writer.Write(string.Join(",", typeof(People).GetProperties().Select(x => x.Name).ToList()));
                        writer.Flush();
                    }
                }
            }
            else
            {
                //TODO: extend exception class to create custom eror meszsage message.
            }

            MessageBox.Show("Downloaded CSV - Includes titles of the required data.");
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
