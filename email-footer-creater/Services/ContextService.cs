﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using email_footer_creater.Enums;
using Newtonsoft.Json;
using System.Data.SQLite;
using System.Xml.Serialization;
using System.Xml;
using System.Reflection;
using email_footer_creater.Object;

namespace email_footer_creater
{
    public class ContextService
    {
        public List<Template> templateList = new List<Template>();

        #region Cruid methods
        public dynamic Load<T>(T t, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON) where T : Type
        {
            var i = getObjectFromType(t);
            string filePath = @"" + dataSource + "/" + GetFileExtension(dataSource, t.Name, dataSourceType);
            bool fileExists = File.Exists(filePath) || (File.Exists(dataSource + "/" + Consts.SQLiteFile)) ? true : false;
            List<T> data = null;

            if (fileExists)
            {
                switch (dataSourceType)
                {
                    case DataSourceTypeEnum.CSV:
                        var e = DeserializeFromCSV(i, filePath);
                        return DeserializeFromCSV(i, filePath);
                    case DataSourceTypeEnum.XML:
                        return DeserializeFromXML(i, filePath);
                    case DataSourceTypeEnum.JSON:
                        return DeserializeFromJSON(i, filePath);
                    case DataSourceTypeEnum.SQLITE:
                        return DeserializeFromSQL(i, dataSource + "/" + Consts.SQLiteFile);
                }
            }
            else { return null; }
            return data as List<T>;
        }
        public bool Create<T>(List<T> t = null, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            //variables
            bool Exists = false;

            //if directory exists
            if (!Directory.Exists(dataSource))
            {
                Directory.CreateDirectory(dataSource);
            }

            Type GenericType = getObjectType(t);

            //get the file path
            string filePath = dataSource + "/" + GetFileExtension(dataSource, GenericType.Name, dataSourceType);

            //if the file does not exist
            if (!File.Exists(filePath))
            {
                switch (dataSourceType)
                {
                    case DataSourceTypeEnum.JSON:
                        File.WriteAllText(filePath, SerializeToJSON(t));
                        Exists = true;
                        break;
                    case DataSourceTypeEnum.XML:
                        File.WriteAllText(filePath, SerializeToXML<List<T>>(t));
                        Exists = true;
                        break;
                    case DataSourceTypeEnum.CSV:
                        File.WriteAllText(filePath, SerializeToCSV(t));
                        Exists = true;
                        break;
                    //Create SQL tables and insert the first lot of data
                    case DataSourceTypeEnum.SQLITE:

                        //set the file path again for SQL as we need a seperate naming convention
                        filePath = dataSource + "/" + Consts.SQLiteFile;

                        //build up connection string object
                        SQLiteConnection sQLiteConnection = new SQLiteConnection();

                        //build db file if it does not exist
                        if (!File.Exists(filePath))
                        {
                            //create the basic file
                            SQLiteConnection.CreateFile(filePath);
                        }
                        //open the connection to the database
                        sQLiteConnection = SQLOpen(sQLiteConnection, filePath);

                        bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, GenericType.Name), sQLiteConnection);

                        if (result)
                        {
                            SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);

                        }
                        else
                        {
                            SQLExecute(SerializeToTable(t), sQLiteConnection);
                            SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);

                        }

                        SQLClose(sQLiteConnection, filePath);

                        Exists = result;

                        break;
                    default:
                        return false;
                }
            }
            else
            {
                Exists = false;
            }
            return Exists;
        }
        public bool Save<T>(List<T> t = null, string dataSource = Consts.DataSource, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            //get the curret type
            Type GenericType = getObjectType(t);

            //get the file path
            string filePath = dataSource + "/" + GetFileExtension(dataSource, GenericType.Name, dataSourceType);

            //in order to save the file path must exist - check this.
            bool fileExists = File.Exists(filePath) ? true : false;

            //set up return variable
            bool hasSaved = false;

            switch (dataSourceType)
            {
                case DataSourceTypeEnum.XML:
                    File.WriteAllText(filePath, SerializeToXML(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.CSV:
                    File.WriteAllText(filePath, SerializeToCSV(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.JSON:
                    File.WriteAllText(filePath, SerializeToJSON(t));
                    hasSaved = true;
                    break;
                case DataSourceTypeEnum.SQLITE:

                    SQLiteConnection sQLiteConnection = new SQLiteConnection();

                    sQLiteConnection = SQLOpen(sQLiteConnection, filePath);

                    bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, GenericType.Name), sQLiteConnection);

                    if (result)
                    {
                        SQLExecuteList(SerializeToInsertCommands(t), sQLiteConnection);
                    }
                    else
                    {
                        hasSaved = false;
                    }

                    SQLClose(sQLiteConnection, filePath);

                    hasSaved = result;
                    break;

            }
            return hasSaved;
        }
        public bool Backup<T>(List<T> t = null, string dataSource = Consts.BackupDirectory, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            bool isBackupDaily = false;
            bool isBackupChanges = false;
            bool hasBackedup = false;
            string LastBackup = "";
            Type GenericType = getObjectType(t);
            string filePath = Consts.DataSource + "/" + GetFileExtension(Consts.DataSource, GenericType.Name, dataSourceType);
            var backupDailyName = GetBackupFileName(DateTime.Now.ToString("yyyy.MM.dd"), Consts.BackupDirectory, Consts.BackupFileNamePrefix + GenericType.Name, GetFileSuffix(FileSuffixEnum.Daily), dataSourceType);
            var backupModificationName = GetBackupFileName(DateTime.Now.ToString("yyyy.MM.dd"), Consts.BackupDirectory, Consts.BackupFileNamePrefix + GenericType.Name, GetFileSuffix(FileSuffixEnum.Changes), dataSourceType);


            //1.ensure the backup directory exists and if not create it.
            if (!Directory.Exists(Consts.BackupDirectory))
            {
                Directory.CreateDirectory(Consts.BackupDirectory);
            }

            //get the last backup if any
            LastBackup = GetLastBackup();

            //2.ensure the datasource exists
            if (File.Exists(filePath))
            {
                //save any changes to the datasource before the backup process
                Save(t, Consts.DataSource, dataSourceType);
            }
            else
            {
                //output why we cannot backup data
                isBackupDaily = false;
                isBackupChanges = false;
                throw new FileNotFoundException("BACKUP STOPPED: File not found");
                //return hasBackedup;
            }

            //3.check to see if there is a daily backup already...
            if (File.Exists(backupDailyName))
            {

                //if the file exists then this is not a daily backup
                isBackupDaily = false;

                //check for changes made by counting objects
                var source = new FileInfo(filePath).Length;
                var backup = new FileInfo(LastBackup).Length;
                if (source > backup || backup < source)
                {
                    isBackupChanges = true;
                }
                else
                {
                    Console.WriteLine("BACKUP STOPPED: No changes made to previous JSON.");
                    isBackupChanges = false;
                    isBackupDaily = false;
                    return hasBackedup;
                }
            }
            else
            {
                isBackupChanges = false;
                isBackupDaily = true;
                Console.WriteLine("BACKUP CONTINUED: No backup made today.");
            }


            if (isBackupChanges)
            {
                //do changes to file
                File.Copy(filePath, backupModificationName);
                return true;
            }
            else if (isBackupDaily)
            {
                //do the daily backup
                File.Copy(filePath, backupDailyName);
                return true;
            }
            else
            {
                //do somthing else
                return false;
            }
        }
        #endregion

        #region Import/Export Mehtods
        //Save Dialouge 
        public string getSaveFormat(DataSourceTypeEnum format)
        {
            Dictionary<string, string> saveFormats = new Dictionary<string, string>();
            //Text files (*.txt)|*.txt|All files (*.*)|*.*"'

            saveFormats.Add(DataSourceTypeEnum.JSON.ToString(), "JSON (*.json)|*.json");
            saveFormats.Add(DataSourceTypeEnum.CSV.ToString(), "Excel (*.csv)|*.csv");
            saveFormats.Add(DataSourceTypeEnum.XML.ToString(), "XML (*.xml)|*.xml");
            saveFormats.Add(DataSourceTypeEnum.SQLITE.ToString(), "SQL (*.sql)|*.sql");
            var I = saveFormats[format.ToString()];
            return I;
        }

        //serialize 
        public string SerializeTo<T>(List<T> list, DataSourceTypeEnum dataSourceTypeEnum = DataSourceTypeEnum.CSV)
        {
            string dataString = "";

            //classify the object,
            switch (dataSourceTypeEnum)
            {
                case DataSourceTypeEnum.CSV:
                    dataString = SerializeToCSV(list);
                    break;
                case DataSourceTypeEnum.JSON:
                    dataString = SerializeToJSON(list);
                    break;
                case DataSourceTypeEnum.XML:
                    dataString = SerializeToXML(list);
                    break;
                case DataSourceTypeEnum.SQLITE:
                    StringBuilder sb = new StringBuilder();
                    foreach (var s in SerializeToInsertCommands(list).ToList())
                    {
                        sb.AppendLine(s.ToString());
                    }
                    dataString = sb.ToString();
                    break;
            }

            return dataString;
        }
        public string SerializeToJSON<T>(List<T> DataList)
        {
            return JsonConvert.SerializeObject(DataList);
        }
        public string SerializeToCSV<T>(List<T> DataList)
        {

            StringBuilder csv = new StringBuilder();

            if (DataList is List<Template>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(Template).GetProperties().Select(x => x.Name).ToArray()));
                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    Template currJob = p.value as Template;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(Template).GetProperties())
                    {
                        row.Add(FormatPropertyLists(p.value, i));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }


            }
            else if (DataList is List<People>)
            {
                //header names for the CSV
                csv.AppendLine(String.Join(",", typeof(People).GetProperties().Select(x => x.Name).ToArray()));
                //write csv lines
                foreach (var p in DataList.Select((value, projectIndex) => new { value, projectIndex }))
                {
                    People currJob = p.value as People;
                    List<string> row = new List<string>();
                    foreach (var i in typeof(People).GetProperties())
                    {
                        row.Add(FormatPropertyLists(p.value, i));
                    }

                    csv.AppendLine(String.Join(",", row.ToArray()));
                }


            }
            return csv.ToString();
        }
        public string SerializeToXML<T>(T filter)
        {

            var serializer = new XmlSerializer(typeof(T));
            string utf8;
            using (StringWriter writer = new Utf8StringWriter())
            {
                serializer.Serialize(writer, filter);
                utf8 = writer.ToString();
            }
            return utf8;
        }

        //deserialize
        public dynamic DeserializeTo(dynamic list, string data, DataSourceTypeEnum dataSourceTypeEnum = DataSourceTypeEnum.CSV)
        {

            //classify the object,
            switch (dataSourceTypeEnum)
            {
                case DataSourceTypeEnum.CSV:
                    return DeserializeFromCSV(list, data);
                case DataSourceTypeEnum.JSON:
                    return DeserializeFromJSON(list, data);
                case DataSourceTypeEnum.XML:
                    return DeserializeFromXML(list, data);
                case DataSourceTypeEnum.SQLITE:
                    break;
            }

            return null;
        }
        private dynamic DeserializeFromCSV<T>(T t, string filePath) where T : class
        {
            var titles = from line in File.ReadAllLines(filePath).Take(1)
                         select (line.Split(','));


            var data = from line in File.ReadAllLines(filePath).Skip(1)
                       select (line.Split(',')).ToArray();


            List<T> list = new List<T>();

            if (t is People)
            {

                People p = new People();
                p.CreatedAt = DateTime.Now;

                List<Dictionary<string, string>> dicList = new List<Dictionary<string, string>>();

                foreach (var line in data)
                {
                    for (int numItem = 0; numItem < titles.ToList()[0].ToList().Count; numItem++)
                        p.Attributes.Add(titles.ToList()[0].ToList()[numItem], line[numItem]);
                }

                list.Add(p as T);
            }



            return list;
        }

        private dynamic DeserializeFromXML<T>(T t, string filePath) where T : class
        {
            var xdoc = File.ReadAllText(filePath);
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute("ArrayOf" + t.GetType().Name));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xdoc));
            return (List<T>)serializer.Deserialize(memStream);
        }
        private dynamic DeserializeFromJSON<T>(T t, string filePath) where T : class
        {
            using (StreamReader file = File.OpenText(filePath))
            {
                return JsonConvert.DeserializeObject<List<T>>(file.ReadToEnd());
            }
        }

        //SQL
        private string SerializeToTable<T>(List<T> dataList)
        {
            StringBuilder sqlcommand = new StringBuilder();
            List<string> sqlcolumns = new List<string>();


            if (dataList is List<Template>)
            {
                foreach (var s in typeof(Template).GetProperties())
                {
                    sqlcolumns.Add(s.Name.ToString());
                }
                sqlcommand.AppendLine(String.Format(Consts.SQLiteCreate, typeof(Template).Name, String.Join(",", sqlcolumns.ToArray())));
            }
            else
            {
                throw new Exception("DataType Missing!") { };
            }
            return sqlcommand.ToString();
        }
        private List<string> SerializeToInsertCommands<T>(List<T> dataList)
        {
            List<string> rows = new List<string>();
            Type obj = null;

            if (dataList is List<Template>)
            {
                obj = typeof(Template);
                foreach (var p in dataList.Select((value, index) => new { value, index }))
                {
                    List<string> property = new List<string>();
                    foreach (var i in typeof(Template).GetProperties())
                    {

                        if (i.GetValue(p.value as Template) is null)
                        {
                            property.Add("null");
                        }
                        else
                        {
                            //TODO: Make this standard: check if property is a list of int and format these as '1|2' to ensure we can parce this when loading in.
                            property.Add(FormatPropertyLists(p.value, i));
                        }

                    }
                    rows.Add(string.Format(Consts.SQLiteInsert, obj.Name, String.Join(", ", typeof(Template).GetProperties().Select(x => x.Name).ToArray().Select(x => "'" + x + "'")), String.Join(", ", property.ToArray().Select(x => "'" + x + "'"))));
                }
            }


            return rows;
        }
        private dynamic DeserializeFromSQL<T>(T t, string filePath) where T : class
        {
            //check that the table exists
            SQLiteConnection sQLiteConnection = new SQLiteConnection();
            sQLiteConnection = SQLOpen(sQLiteConnection, filePath);
            bool result = SQLCheckTableExists(String.Format(Consts.SQLiteCheckTableExists, t.GetType().Name), sQLiteConnection);
            if (result)
            {
                string query = String.Format(Consts.SQLiteSelect, t.GetType().Name);
                return SQLExecuteForObjects(t, query, sQLiteConnection);
            }
            return "";
        }

        //other sql
        private SQLiteConnection SQLOpen(SQLiteConnection m_dbConnection, string dbPath)
        {
            m_dbConnection =
            new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");
            m_dbConnection.Open();
            return m_dbConnection;
        }
        private void SQLClose(SQLiteConnection m_dbConnection, string dbPath)
        {
            m_dbConnection =
            new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");
            m_dbConnection.Open();
        }
        private void SQLExecute(string sql, SQLiteConnection sQLiteConnection)
        {
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            command.ExecuteReader();
        }
        private dynamic SQLExecuteForObjects<T>(T t, string sql, SQLiteConnection sQLiteConnection) where T : class
        {
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            List<T> objList = new List<T>();
            while (reader.Read())
            {
                if (t is Template)
                {
                    objList.Add(new Template()
                    {
                        Path = (string)reader["Path"],
                        Tag = Convert.ToString((string)reader["Tags"]).Split(';').Select(part => part.Split('=')).Where(part => part.Length == 2).ToDictionary(sp => sp[0], sp => sp[1]),
                    } as T);
                }
            }
            return objList;
        }
        private void SQLExecuteList(List<string> CommandList, SQLiteConnection sQLiteConnection)
        {
            foreach (string command in CommandList)
            {
                SQLiteCommand sql = new SQLiteCommand(command, sQLiteConnection);
                sql.ExecuteNonQuery();
            }
        }
        private bool SQLCheckTableExists(string sql, SQLiteConnection sQLiteConnection)
        {
            bool exists = false;
            SQLiteCommand command = new SQLiteCommand(sql, sQLiteConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                exists = true;
            }
            return exists;
        }

        //xml settings
        private XmlWriterSettings GetXMLSettings()
        {
            return new XmlWriterSettings()
            {
                Indent = false,
                ConformanceLevel = ConformanceLevel.Auto,
            };
        }

        //types, mapping formatting
        public dynamic getObjectFromType(Type t)
        {
            if (t == typeof(Template)) { return new Template() { }; }
            else if (t == typeof(People)) { return new People() { }; }

            else { throw new Exception($"No object type here for {t.Name}"); };
        }
        public Type getObjectType<T>(T dataList)
        {
            Type obj = null;
            if (dataList is List<Template> || dataList is Template) { obj = typeof(Template); }
            else if (dataList is List<People> || dataList is People) { obj = typeof(People); }
            else { obj = null; }
            return obj;
        }
        public object mapObjectProperites<T>(List<T> dataList)
        {
            List<T> list = new List<T>();
            foreach (var p in dataList.Select((value, projectIndex) => new { value, projectIndex }))
            {
                list.Add(p.value);
            }

            return list;

        }

        private string FormatPropertyLists(object p, PropertyInfo i)
        {
            //save a list of ints as a seperated string
            if (i.PropertyType == typeof(List<int>))
            {
                List<int> l = i.GetValue(p as Template) as List<int>;
                if (l != null)
                {
                    List<string> line = new List<string>();
                    foreach (var e in l)
                    {
                        line.Add(e.ToString());
                    }
                    return String.Join("|", line.ToArray());
                }
                return "";
            }


            return Convert.ToString(i.GetValue(p as People));


        }
        #endregion

        #region File Information Methods
        public string GetFileExtension(string dataSource, string genericObject, DataSourceTypeEnum dataSourceType)
        {
            var fileSuffix = "." + dataSourceType;
            var filePrefix = genericObject + "_";
            bool hasType = (genericObject.ToString().Length > 0) ? true : false;

            if (genericObject.ToString().Length <= 0)
            {
                return "";
            }

            if (dataSource.Contains(fileSuffix))
            {
                return filePrefix + dataSource;
            }
            else
            {
                return filePrefix + dataSource + fileSuffix;
            }
        }
        public string GetLastBackup()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Consts.BackupDirectory);

            if (directoryInfo.GetFiles().Length > 0)
            {
                var r = directoryInfo.GetFiles().OrderByDescending(f => f.LastWriteTime).First().FullName;
                if (r != null)
                {
                    return r;
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }

        }
        public string GetFileSuffix(FileSuffixEnum fileSuffix = FileSuffixEnum.Daily)
        {
            string suffix = "";

            switch (fileSuffix)
            {
                case FileSuffixEnum.Daily:
                    suffix = "(Daily)";
                    break;
                case FileSuffixEnum.Changes:
                    suffix = "(Changes) " + DateTime.Now.ToString("hh-mm tt");
                    break;
            }

            return suffix;
        }
        public string GetBackupFileName(string Date, string backupDirectory = Consts.BackupDirectory, string backupNamePrefix = Consts.BackupFileNamePrefix, string backupNameSuffix = null, DataSourceTypeEnum dataSourceType = DataSourceTypeEnum.JSON)
        {
            return String.Format(Consts.BackupFileNameTemplate, backupDirectory, backupNamePrefix, Date, backupNameSuffix.ToString(), dataSourceType.ToString());
        }
        #endregion

    }
}
