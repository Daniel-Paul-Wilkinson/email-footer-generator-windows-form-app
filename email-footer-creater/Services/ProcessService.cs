﻿using email_footer_creater.Enums;
using email_footer_creater.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Services
{
    public class ProcessService
    {
        /// <summary>c
        /// Opens a file in chrome
        /// </summary>
        /// <param name="app">Application to open in</param>
        /// <param name="path">Path to a file to open</param>
        public void RunProcess(string path, ApplicationTypeEnum applicationTypeEnum = ApplicationTypeEnum.Chrome)
        {
            if (path != null && path.Length > 0)
            {
                ProcessStartInfo info = new ProcessStartInfo(EnumHelper.GetApplicationType(applicationTypeEnum));
                info.CreateNoWindow = true;
                info.UseShellExecute = false;
                info.Arguments = path;
                Process.Start(info);
            }
        }
    }
}
