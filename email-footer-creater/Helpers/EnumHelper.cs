﻿using email_footer_creater.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace email_footer_creater.Helpers
{
    class EnumHelper
    {
        public static string GetApplicationType(ApplicationTypeEnum application)
        {
            string app = "";

            switch (application)
            {
                case ApplicationTypeEnum.Chrome:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.Edge:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.FireFox:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.IE:
                    app = @"C:\Program Files (x86)\Internet Explorer\iexplore.exe";
                    break;
            }

            return app;
        }
    }
}
